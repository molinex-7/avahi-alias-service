# Avahi Alias

Made by Thiago Molina with technologies like BASH, UNIX utilities, and avahi.

Finally a little shell script

### Description

Do you know what Apache is? So you also know what vhosts are.

It's cool to have some vhosts for every project we're working on. And sometimes 
it's cool to also access our projects from other machines in our network, to see 
how they behave on other devices. We are talking here, of a development environment, 
not production, that is, no DNS server.

To summarize the story, Apache has a module, which publishes aliases on the local 
network, if I'm not mistaken, called mod_dnssd. But I could not make it work alone, 
without the help in our zero config service. In the case the avahi-daemon. So what 
does this script do?

Do the magic. It publishes the aliases in the local network, and we can access our 
projects, with a url like this:

	http://test.local

What is much better than looking for our project by host name, or worse, by the IP 
of the machine. And it also publishes more than one alias, that is, we can create 
an alias for each vhost.

### Install

Imagining that you have apache, and mod_dnssd, installed, and enabled, and also 
avahi-daemon, we start with the works.

First, clone (and collaborate, hopefully forks) that repository, or download. 
Enter the directory, and now as root, move the script to:

	# mv avahi-alias /usr/local/bin/

From the execution permission to the script:

	# chmod +x /usr/local/bin/avahi-alias

Then move the unit to:

	# mv avahi-aliasd.service /usr/lib/systemd/system/avahi-aliasd.service

We are almost done, now create a directory if it does not exist in:

	# mkdir -p /etc/avahi/alias.d

And inside it, create a file

	# touch /etc/avahi/alias.d/alias

Okay, inside this file we'll write the aliases we want the script to publish. 
One per line, and following this model:

	projectname.local

### Tip

If you use firewall (and, of course, you should), open the gates to hell (ports 80, 
443 if you are certified for apache, and port 5353 for avahi-daemon), and this just 
for your local network. Do not go opening those doors to the world huh!

Without this, the script does not work

### Run it

Enable and start the service

	# systemctl enable avahi-aliasd && systemctl start avahi-aliasd

After 5 minutes it started publishing the aliases on the network...

PS: Every time you boot the machine, it takes 5 minutes to start working, no wonder.

### Contact

For any clarification contact us

	Mail: t.molinex@gmail.com

